<?php
namespace Swissclinic\Checkout\Block\Widget;

use Magento\Checkout\Block\Cart\Crosssell;
use Magento\Widget\Block\BlockInterface;

/**
 * Related Products Widget Class
 * @package Swissclinic\Checkout\Block\Widget
 */
class Related extends Crosssell implements BlockInterface
{
    protected function _construct()
    {
        $this->_template = 'Swissclinic_Checkout::product/list/items.phtml';
        parent::_construct();
    }
}