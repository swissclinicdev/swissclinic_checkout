<?php

namespace Swissclinic\Checkout\Block\Checkout;

use Magento\Framework\View\Element\Template;
use Swissclinic\Checkout\Helper\Data;

class Header extends Template {

    protected $helper;

    public function __construct(
        Data $helper,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    )
    {
        $this->helper = $helper;
        
        parent::__construct($context, $data);
    }
    
    public function isDiscountFieldEnabled() 
    {
        return $this->helper->isDiscountFieldEnabled();
    }
}