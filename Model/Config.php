<?php

namespace Swissclinic\Checkout\Model;


class Config {
    /**
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
    protected $scopeConfig;

    /**
    * @var \Magento\Store\Model\StoreManagerInterface
    */
    protected $storeManager;

    /**
    * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    */
    public function __construct(
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    const CHECKOUT_SWISSCLINIC_CHECKOUT_ENABLED = 'swissclinic_checkout/swissclinic_checkout_config/enabled';
    const CHECKOUT_SWISSCLINIC_FREE_SHIPPING_ENABLED = 'swissclinic_checkout/swissclinic_checkout_config/onlyfreeshipping';
    const CHECKOUT_SWISSCLINIC_DISCOUNT_CODE_DISABLED = 'swissclinic_checkout/swissclinic_checkout_config/disableDiscountCodeField';

    /**
    * @param null $store
    * @return mixed
    */
    public function isEnabled($store = null)
    {
        return $this->scopeConfig->getValue(self::CHECKOUT_SWISSCLINIC_CHECKOUT_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

    public function isOnlyFreeShippingEnabled($store = null)
    {
        return $this->scopeConfig->getValue(self::CHECKOUT_SWISSCLINIC_FREE_SHIPPING_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }
    
    public function isDiscountFieldEnabled($store = null)
    {
        return !$this->scopeConfig->getValue(self::CHECKOUT_SWISSCLINIC_DISCOUNT_CODE_DISABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

}