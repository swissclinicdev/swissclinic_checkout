<?php
namespace Swissclinic\Checkout\Model\Config\Source\Widget;

use Magento\Framework\Option\ArrayInterface;

class RelationTypes implements ArrayInterface {

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray() {
        return [
            ['value' => null, 'label' => '-- Please Select --'],
            ['value' => 'crosssell', 'label' => 'Crosssell'],
            ['value' => 'upsell', 'label' => 'Upsell'],
            ['value' => 'related', 'label' => 'Related'],
        ];
    }
}