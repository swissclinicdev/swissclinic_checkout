<?php
/**
 * Copyright © 2017 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Swissclinic\Checkout\Model\Config\Source;

use Swissclinic\Checkout\Helper\Data as Helper;

/**
 * Class SubscribeLayoutProcessor
 */
class SwissclinicCheckoutLayoutProcessor {

    private $_helper;


    public function __construct(
        Helper $_helper
    )
    {
        $this->_helper = $_helper;
    }

    public function process($jsLayout)
    {
        return $jsLayout;
    }
}
