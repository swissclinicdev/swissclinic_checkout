<?php
namespace Swissclinic\Checkout\Helper;

use Swissclinic\Checkout\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    protected $_config;

    protected $storeManager;

    protected $scopeConfig;

    public function __construct(
        Config $config,
        Context $context,
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->_config = $config;
    }

    public function isEnabled(){
        $store = $this->storeManager->getStore();
        return	$this->_config->isEnabled($store);
    }

    public function isOnlyFreeShippingEnabled(){
        $store = $this->storeManager->getStore();
        return	$this->_config->isOnlyFreeShippingEnabled($store);
    }
    
    public function isDiscountFieldEnabled(){
        $store = $this->storeManager->getStore();
        return	$this->_config->isDiscountFieldEnabled($store);
    }
    
}
