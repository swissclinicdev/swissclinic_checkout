<?php

namespace Swissclinic\Checkout\Plugin;

use Magento\Catalog\Api\ProductRepositoryInterface;

class CartTotalRepository
{
    private $_productRepository;

    private $_session;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        \Magento\Checkout\Model\Session $session
    ) {
        $this->_productRepository = $productRepository;
        $this->_session = $session;
    }

    public function getPrice($sku) {
        $product = $this->_productRepository->get($sku);
        return $product->getPrice();
    }

    public function afterGet(
        \Magento\Quote\Model\Cart\CartTotalRepository $subject,
        \Magento\Quote\Api\Data\TotalsInterface $result
    ) {
        $quoteItems = $this->_session->getQuote()->getAllVisibleItems();
        $totalItems = $result->getItems();
        if(!$totalItems)
        {
            return $result;
        }
        foreach ($totalItems as $index => $item) {
            if ($item->getExtensionAttributes() === null) {
                $extensionAttributes = $this->extensionFactory->create();
                $item->setExtensionAttributes($extensionAttributes);
            }

            $originalPrice = $this->getPrice($quoteItems[$index]->getSku());

            $extensionAttributes = $item->getExtensionAttributes();
            $extensionAttributes->setOriginalPrice($originalPrice);
            $item->setExtensionAttributes($extensionAttributes);
        }

        return $result;
    }
}
