<?php

namespace Swissclinic\Checkout\Plugin\Model\Rate;

use Swissclinic\Checkout\Helper\Data as Helper;

class Result
{
    private $_helper;

    private $_freecarrierIndex = null;

    public function __construct(
        Helper $helper
    )
    {
        $this->_helper = $helper;
    }

    public function afterGetAllRates(\Magento\Shipping\Model\Rate\Result $subject, $result)
    {
        if ($this->_helper->isOnlyFreeShippingEnabled()) {
            if (sizeof($result) > 1) {
                $index = 0;
                foreach ($result as $rate) {
                    $data = $rate->getData();

                    if ($data['carrier'] == "freeshipping") {
                        $this->_freecarrierIndex = $index;
                    }
                    $index++;
                }
            }

            if (isset($this->_freecarrierIndex)) {
                return array_slice($result, $this->_freecarrierIndex, 1);
            }
        }

        return $result;
    }
}