var config = {
    map: {
        '*': {
            'Magento_Checkout/js/view/summary/grand-total': 'Swissclinic_Checkout/js/view/summary/grand-total',
            'Magento_Checkout/js/view/summary/item/details/subtotal': 'Swissclinic_Checkout/js/view/summary/item/details/subtotal',
            'swissclinic/update-item-qty': 'Swissclinic_Checkout/js/action/update-item-qty'
        }
    }
};