define([
    'jquery',
    'loader'
], function ($) {
    'use strict';

    return function(config, element) {
        var $form = $(element).closest('#form-validate');
        $(element).change(function() {
            $form[0].submit()
        });
    };
});
