/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote'
], function (Component, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Swissclinic_Checkout/summary/discount_amount'
        },

        /**
         * @return {*}
         */
        isDisplayed: function () {
            var totals = quote.getTotals()();

            if (totals) {
                return totals['discount_amount'] < 0;
            }

            return quote['discount_amount'] < 0;
        },

        /**
         * Get pure value.
         */
        getPureValue: function () {
            var totals = quote.getTotals()();

            if (totals) {
                //changed from grand_total to base_grand_total since grand_total removes tax
                return totals['discount_amount'];
            }
            //changed from grand_total to base_grand_total since grand_total removes tax
            return quote['discount_amount'];
        },

        /**
         * @return {*|String}
         */
        getValue: function () {
            return this.getFormattedPrice(this.getPureValue());
        }
    });
});
