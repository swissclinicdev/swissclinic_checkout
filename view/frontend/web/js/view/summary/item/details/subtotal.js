/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Checkout/js/view/summary/abstract-total'
], function (viewModel) {
    'use strict';

    return viewModel.extend({
        defaults: {
            displayArea: 'after_details',
            template: 'Swissclinic_Checkout/summary/item/details/subtotal'
        },

        /**
         * @param {Object} quoteItem
         * @return {*|String}
         */
        getValue: function (quoteItem) {
            return this.getFormattedPrice(quoteItem['row_total_incl_tax']);
        },


        /**
         * @param {Object} quoteItem
         * @return {*|Boolean}
         */
        isOriginalDisplayed: function (quoteItem) {

            var total = parseInt(quoteItem['price_incl_tax']);

            var original = parseInt(quoteItem['extension_attributes']['original_price']);

            return total < original;
        },

        /**
         * @param {Object} quoteItem
         * @return {*|String}
         */
        getOriginal: function (quoteItem) {
            var rowOriginalTotal = quoteItem['extension_attributes']['original_price'] * quoteItem['qty'];
            return this.getFormattedPrice(rowOriginalTotal);
        }
    });
});
